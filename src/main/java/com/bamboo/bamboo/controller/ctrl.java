package com.bamboo.bamboo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class ctrl {
    @GetMapping("bamboo")
    public void bamboo(HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            out.println("Bamboo");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
